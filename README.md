# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://anze-gn@bitbucket.org/anze-gn/stroboskop.git
```

Naloga 6.2.3:
[Priprava potrebnih JavaScript knjižnic](https://bitbucket.org/anze-gn/stroboskop/commits/94dd555b5c53e695037bbc1a707df112cd14fc90)

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
[Dodajanje stilov spletne strani](https://bitbucket.org/anze-gn/stroboskop/commits/83abc64ea95fce453dd5f364237c319afb263c93)

Naloga 6.3.2:
[Desna poravnava besedil](https://bitbucket.org/anze-gn/stroboskop/commits/6f932012e2f042326b300a30260a8813ffc3d9dd)

Naloga 6.3.3:
[Dodajanje gumba za dodajanje barv](https://bitbucket.org/anze-gn/stroboskop/commits/4037999090c81fc9c3a5c924eedef4fd62be6cfe)

Naloga 6.3.4:
[Dodajanje robov in senc](https://bitbucket.org/anze-gn/stroboskop/commits/26d7539b276e9bb80aeccd94faa85bdc884225e8)

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
[Dodajanje izbiranja barv](https://bitbucket.org/anze-gn/stroboskop/commits/c2a8fa688739fbccdeec88468e976ab99cac453a)

Naloga 6.4.2:
[Dinamika gumba za dodajanje barv](https://bitbucket.org/anze-gn/stroboskop/commits/33bf941311b71eba13bc5475307e64a64b383627)

Naloga 6.4.3:
[Dinamika gumba za ustavitev stroboskopa](https://bitbucket.org/anze-gn/stroboskop/commits/02ee72acd672d2a4910070467c140164e5f9e77e)

Naloga 6.4.4:
[Dinamika gumba za zagon stroboskopa](https://bitbucket.org/anze-gn/stroboskop/commits/fbb71a2681d279ad173a27d925d7ad7b2f3ee0be)